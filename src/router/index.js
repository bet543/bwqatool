import Vue from 'vue'
import Router from 'vue-router'
import HistoryGenerator from '@/components/HistoryGenerator'
import HostProfile from '@/components/HostProfile'
import VueSidebarMenu from 'vue-sidebar-menu'
import 'vue-sidebar-menu/dist/vue-sidebar-menu.css'
Vue.use(Router)
Vue.use(VueSidebarMenu)

export default new Router({
	routes: [{
		path: '/',
		component: null
	},{
		path: '/HistoryGenerator/',
		component: HistoryGenerator
	},{
		path: '/HostProfile/',
		component: HostProfile
	}]
})
