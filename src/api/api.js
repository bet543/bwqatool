import {
	Observable
} from 'rxjs';
import axios from 'axios';
import { createInstance } from './base';

export default {
	fetchSettings() {
		return axios.get('./config/settings.json');
	},
	queryGameList(cb) {
		const ob = Observable.create((observer) => {
			axios.get("http://103.69.206.115:7771/api/v1/games").then((response) => {
				observer.next(response.data);
				observer.complete();
			}).catch((error) => {
				observer.error(error);
			})
		});

		const subscription = ob.subscribe({
			next: data => {
				console.log("[game data] => ", data);
				cb(null, data);
			},
			error: err => {
				cb(err)
			},
			complete: data => {
				console.log("[complete]");
				subscription.unsubscribe();
			}
		})
	},
	queryConfig(cb) {
		const ob = Observable.create((observer) => {
			axios.get("http://de1-cdn.allstar-interactive.com/games/launcher/configs/index.json").then((response) => {
				observer.next(response.data);
				observer.complete();
			}).catch((error) => {
				observer.error(error);
			})
		});

		const subscription = ob.subscribe({
			next: data => {
				console.log("[game data] => ", data);
				cb(null, data);
			},
			error: err => {
				cb(err)
			},
			complete: data => {
				console.log("[complete]");
				subscription.unsubscribe();
			}
		})
	},
	queryHostList(cb) {
		const ob = Observable.create((observer) => {
			axios.get("/api/v1/host").then((response) => {
				observer.next(response.data);
				observer.complete();
			}).catch((error) => {
				observer.error(error);
			})
		});

		const subscription = ob.subscribe({
			next: data => {
				console.log("[host data] => ", data);
				cb(null, data);
			},
			error: err => {
				cb(err)
			},
			complete: data => {
				console.log("[complete]");
				subscription.unsubscribe();
			}
		})
	},
	queryHost(param, cb) {
		const ob = Observable.create((observer) => {
			axios.get("/api/v1/host/" + param).then((response) => {
				observer.next(response.data);
				observer.complete();
			}).catch((error) => {
				observer.error(error);
			})
		});

		const subscription = ob.subscribe({
			next: data => {
				console.log("[host info data] => ", data);
				cb(null, data);
			},
			error: err => {
				cb(err)
			},
			complete: data => {
				console.log("[complete]");
				subscription.unsubscribe();
			}
		})
	},
	updateHostProfile(param, cb) {
		const ob = Observable.create((observer) => {
			axios.put("/api/v1/host", param).then((response) => {
				observer.next(response.data);
				observer.complete();
			}).catch((error) => {
				observer.error(error);
			})
		});

		const subscription = ob.subscribe({
			next: data => {
				console.log("[host info data] => ", data);
				cb(null, data);
			},
			error: err => {
				cb(err)
			},
			complete: data => {
				console.log("[complete]");
				subscription.unsubscribe();
			}
		})
	},
	createHostProfile(param, cb) {
		const ob = Observable.create((observer) => {
			axios.post("/api/v1/host", param).then((response) => {
				observer.next(response.data);
				observer.complete();
			}).catch((error) => {
				observer.error(error);
			})
		});

		const subscription = ob.subscribe({
			next: data => {
				console.log("[host info data] => ", data);
				cb(null, data);
			},
			error: err => {
				cb(err)
			},
			complete: data => {
				console.log("[complete]");
				subscription.unsubscribe();
			}
		})
	},
}
