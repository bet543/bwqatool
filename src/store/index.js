import Vue from 'vue'
import Vuex from 'vuex'
import api from "../api/api";

const debug = process.env.NODE_ENV !== 'production';
Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		envList: [{
				env: "dev",
				url: "http://de1-cdn.allstar-interactive.com/games/History_Client"
			},
			{
				env: "qa",
				url: "http://qa-cdn.allstar-interactive.com/games/History_Client"
			},
			{
				env: "sandbox3",
				url: "https://sandbox3-cdn.luckytwist.com/games/History_Client"
			},
			{
				env: "sandbox2",
				url: "https://sandbox2-cdn.luckytwist.com/games/History_Client"
			},
		],
		language: ["en-us", "zh-cn", "ja-jp", "ko-kr", "th-th"],
		currency: ["CNY", "USD", "EUR", "THB", "JPY", "MYR", "KRW", "IDR", "RUB", "INR", "VND"],
		product: ["BackOffice", "FireDice", "Live", "Cascading"],
		gameName: [],
		hostList: [],
		hostInfo: null,
	},
	getters: {
		getEnvList(state, getters, rootState) {
			return state.envList;
		},
		getLanguage(state, getters, rootState) {
			return state.language;
		},
		getCurrency(state, getters, rootState) {
			return state.currency;
		},
		getGameName(state, getters, rootState) {
			return state.gameName;
		},
		getProduct(state, getters, rootState) {
			return state.product;
		},
		getHostList(state, getters, rootState) {
			return state.hostList;
		},
		getHostInfo(state, getters, rootState) {
			return state.hostInfo;
		},
	},
	actions: {
		// 取得所有遊戲
		queryGameList: ({
			commit,
			dispatch
		}) => {
			api.queryGameList((err, data) => {
				commit("setGameList", data)
			})
		},
		queryConfig: ({
			commit,
			dispatch
		}) => {
			api.queryConfig((err, data) => {
				commit("setConfig", data)

			})
		},
		// 主播清單
		queryHostList: ({
			commit,
			dispatch
		}) => {
			api.queryHostList((err, data) => {
				commit("setData", {
					key: "hostList",
					data
				});
			})
		},
		// 取得主播資訊
		queryHost: ({
			commit,
			dispatch
		}, payload) => {
			api.queryHost(payload, (err, data) => {
				commit("setData", {
					key: "hostInfo",
					data
				});
			})
		},
		// 更新主播檔案
		updateHostProfile: ({
			commit,
			dispatch
		}, payload) => {
			api.updateHostProfile(payload, (err, data) => {

			})
		},
		// 新增主播檔案
		createHostProfile: ({
			commit,
			dispatch
		}, payload) => {
			return api.createHostProfile(payload, (err, data) => {
				const { result, errorMsg } = data;
				if(result !== 1) {
					alert(errorMsg);
					return;
				}
				dispatch("queryHostList");
			})
		},
		initHostInfo: ({
			commit,
			dispatch
		}) => {
			commit("setData", {
				key: "hostInfo",
				data: {
					imgUrl: {
						head: {
							def: ""
						},
						profile: {
							def: "",
						}
					},
					content: {
						"zh-cn": {
							name: "",
							detail: [],
							talks: "",
						}
					}
				}
			})
		},
		clear: ({
			commit,
			dispatch
		}) => {
			commit("setData", {
				key: "hostInfo",
				data: null
			})
		},
	},
	mutations: {
		setGameList: (state, data) => {
			state.gameName = data.map((item) => {
				return item.ID;
			});
		},
		setConfig: (state, data) => {
			const {
				currencyList,
				langList
			} = data;
			state.currency = currencyList;
			state.language = langList;
		},
		setData: (state, {
			key,
			data
		}) => {
			console.log("setData " + key + " = ", data);
			state[key] = data;
			console.log("state = ", state);
		},
	},
	strict: debug,
})
