// 可能是我的node版本问题，不用严格模式使用ES6语法会报错
// const models = require('./db');
const express = require('express');
const router = express.Router();
const fileSys = require("../helper/fileSys");

// const profileFolderPath = "./server/profile";
const profileFolderPath = "/var/www/html/cdn/de1-cdn.allstar-interactive.com/host-game-assets/profile";
// const profileFilePath = `${profileFolderPath}/{name}/{name}.json`;
const profileFilePath = `${profileFolderPath}/{name}/{name}.json`;

const regex = new RegExp(/{name}/, 'g');
const sftp = require("../helper/sftp");
// const projectKey = "BG";

// const client = new Client('http://bb.allstar-interactive.com:7990', auth);
/************** 创建(create) 读取(get) 更新(update) 删除(delete) **************/

// 创建账号接口
// router.post('/api/login/createAccount', (req, res) => {
// 	// 这里的req.body能够使用就在index.js中引入了const bodyParser = require('body-parser')
// 	let newAccount = new models.Login({
// 		account: req.body.account,
// 		password: req.body.password
// 	});
// 	// 保存数据newAccount数据进mongoDB
// 	newAccount.save((err, data) => {
// 		if (err) {
// 			res.send(err);
// 		} else {
// 			res.send('createAccount successed');
// 		}
// 	});
// });
// 获取已有账号接口
// router.get('/api/login/getAccount', (req, res) => {
// 	// 通过模型去查找数据库
// 	models.Login.find((err, data) => {
// 		if (err) {
// 			res.send(err);
// 		} else {
// 			res.send(data);
// 		}
// 	});
// });

router.get("/api/v1/host", (req, res) => {
	// local folder
	// res.send(fileSys.getAllFolders(profileFolderPath));

	// sfrp folder
	sftp.list(profileFolderPath).then((dir) => {
		console.log("dir", dir);
		res.send(dir.filter(item => item.type === "d").map((item) => item.name));
	}).catch(err => {
		console.error(err.message);
	});

});

router.get("/api/v1/host/:id", (req, res) => {
	const {
		id
	} = req.params;

	// local folder
	// fileSys.getFile(profileFilePath.replace(regex, id)).then((data) => {
	// 	res.send(JSON.parse(data));
	// })

	fileSys.ensureDirectoryExistence(`./server/profile/${id}.json`);

	// sfrp folder
	sftp.get(profileFilePath.replace(regex, id), `./server/profile/${id}.json`).then((dir, gg) => {
		console.log("dir", dir);
		fileSys.getFile("./server/profile/{name}.json".replace(regex, id)).then((data) => {
			res.send(JSON.parse(data));
		})
	}).catch(err => {
		console.error(err.message);
	});
});

router.put("/api/v1/host", (req, res) => {
	const {
		name,
		data
	} = req.body;

	// local folder
	// fileSys.saveFile(profileFilePath.replace(regex, name), data).then(() => {
	// 	res.send({
	// 		result: 1,
	// 	});
	// })

	// sfrp folder
	fileSys.saveFile(`./server/profile/${name}.json`, data).then(() => {
		return sftp.put(`./server/profile/${name}.json`, profileFilePath.replace(regex, name));
	}).then((r) => {
		res.send({
			result: 1,
		});
	}).catch(err => {
		console.error(err.message);
	});
});

router.post("/api/v1/host", (req, res) => {
	const {
		name,
		data
	} = req.body;
	fileSys.createFile(`./server/profile/${name}.json`, data).then(() => {
		return sftp.createDirectory(`${profileFolderPath}/${name}`);
	}).then(() => {
		return sftp.put(`./server/profile/${name}.json`, profileFilePath.replace(regex, name));
	}).then((r) => {
		res.send({
			result: 1,
		});
	}).catch(err => {
		console.error(err.message);
		res.send({
			result: 0,
			errorMsg: err.message
		})
	});
});

module.exports = router;
