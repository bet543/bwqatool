const sftp = require('ssh2-sftp-client'); //连接FTP
const path = require('path');
const fs = require('fs');

const client = new sftp();

client.connect({
	host: '172.16.13.10', // de1
	port: '22',
	user: 'jenkins',
	password: 'R3l3@seM3!!',
}).then(() => {
	console.log("ftp is connected");
}).catch((err) => {
	console.log(err);
})

module.exports = {
	// 目錄列表
	list(dirpath) {
		return client.list(dirpath);
	},
	// 切換目錄
	cwd(dirpath) {
		return client.cwd(dirpath)
	},
	// 下載文件
	get(remotePath, dst) {
		return client.get(remotePath, dst)
	},
	// 上傳文件
	put(currentFile, targetFilePath) {
		return client.put(currentFile, targetFilePath);
	},
	createDirectory(remoteDir) {
		return client.mkdir(remoteDir, true);
	}
}
