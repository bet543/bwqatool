const fs = require('fs');
const path = require('path');

const ensureDirectoryExistence = (filePath) => {
	const dirname = path.dirname(filePath);
	if (fs.existsSync(dirname)) {
		return true;
	}
	ensureDirectoryExistence(dirname);
	fs.mkdirSync(dirname);
}

module.exports = {
	ensureDirectoryExistence,
	getAllFiles: (dir) => {
		return new Promise((resolve, reject) => {
			let list = [];
			fs.readdir(dir, (err, files) => {
				files.forEach(file => {
					list.push(file);
				});
				resolve(list);
			});
		})
	},
	getAllFolders: (dir) => {
		const isDirectory = (source) => {
			return fs.lstatSync(source).isDirectory();
		}
		return fs.readdirSync(dir).filter(name => {
			return isDirectory(path.join(dir, name));
		})
	},
	getFile: (path) => {
		return new Promise((resolve, reject) => {
			fs.readFile(path, "utf8", (err, jsonData) => {
				if (err) {
					console.log(`read ${path} error: ${err}`);
					reject(err);
				} else {
					resolve(jsonData)
				}
			});
		});
	},
	saveFile: (path, data) => {
		return new Promise((resolve, reject) => {
			fs.writeFile(path, JSON.stringify(data, null, 4), (err, jsonData) => {
				if (err) {
					console.log(`read ${path} error: ${err}`);
					reject(err);
				} else {
					resolve()
				}
			});
		});
	},
	createFile: (path, data) => {
		ensureDirectoryExistence(path);
		return new Promise((resolve, reject) => {
			fs.writeFile(path, JSON.stringify(data, null, 4), {
				flag: "wx"
			}, (err, jsonData) => {
				if (err) {
					console.log(`read ${path} error: ${err}`);
					reject(err);
				} else {
					resolve()
				}
			});
		});
	},
}
